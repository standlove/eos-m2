<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<groupId>com.tmobile.iam</groupId>
	<artifactId>iam-customer-profile-rest-client</artifactId>
	<version>1.0.0</version>
	<packaging>jar</packaging>

	<name>iam-customer-profile-rest-client</name>
	<description>IAM Customer Profile models</description>

	<parent>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-parent</artifactId>
		<version>1.5.1.RELEASE</version>
		<relativePath/>
	</parent>

	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
		<java.version>1.8</java.version>
		<maven.compiler.source>1.8</maven.compiler.source>
		<maven.compiler.target>1.8</maven.compiler.target>
		<maven-plugin-version>1.0.0</maven-plugin-version>
	</properties>

	<dependencies>
		<dependency>
			<groupId>com.fasterxml.jackson.core</groupId>
			<artifactId>jackson-annotations</artifactId>
		</dependency>
	</dependencies>

	<build>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<configuration>
					<source>${java.version}</source>
					<target>${java.version}</target>
				</configuration>
			</plugin>

			<plugin>
				<groupId>org.jfrog.buildinfo</groupId>
				<artifactId>artifactory-maven-plugin</artifactId>
				<version>2.6.0</version>
				<inherited>false</inherited>
				<executions>
					<execution>
						<id>build-info</id>
						<goals>
							<goal>publish</goal>
						</goals>
						<configuration>
							<deployProperties>
								<gradle>awesome</gradle>
							</deployProperties>
							<publisher>
								<contextUrl>http://artifactory.corporate.t-mobile.com/artifactory</contextUrl>
								<username>${username}</username>
								<password>${password}</password>
								<excludePatterns>*-tests.jar</excludePatterns>
								<repoKey>eos-release-local</repoKey>
								<snapshotRepoKey>eos-snapshot-local</snapshotRepoKey>
							</publisher>
							<licenses>
								<autoDiscover>true</autoDiscover>
								<includePublishedArtifacts>false</includePublishedArtifacts>
								<runChecks>true</runChecks>
								<scopes>compile,runtime</scopes>
								<violationRecipients>build@organisation.com</violationRecipients>
							</licenses>
						</configuration>
					</execution>
				</executions>
			</plugin>
		</plugins>

		<pluginManagement>
			<plugins>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-compiler-plugin</artifactId>
					<configuration>
						<source>${java.version}</source>
						<target>${java.version}</target>
					</configuration>
				</plugin>
			</plugins>
		</pluginManagement>
	</build>

	<repositories>
		<repository>
			<id>release</id>
			<name>prd-art-jav-2a.corporate.t-mobile.com-release</name>
			<url>http://artifactory.corporate.t-mobile.com/artifactory/eos-release-local</url>
		</repository>
		<repository>
			<id>snapshot</id>
			<name>prd-art-jav-2a.corporate.t-mobile.com-snapshot</name>
			<url>http://artifactory.corporate.t-mobile.com/artifactory/eos-snapshot-local</url>
		</repository>
		<repository>
			<id>spring-snapshots</id>
			<name>Spring Snapshots</name>
			<url>https://repo.spring.io/snapshot</url>
			<snapshots>
				<enabled>true</enabled>
			</snapshots>
		</repository>
		<repository>
			<id>spring-milestones</id>
			<name>Spring Milestones</name>
			<url>https://repo.spring.io/milestone</url>
			<snapshots>
				<enabled>true</enabled>
			</snapshots>
		</repository>
	</repositories>
	
	<distributionManagement>
        <repository>
            <id>release</id>
            <name>prd-art-jav-1a.corporate.t-mobile.com-releases</name>
            <url>https://artifactory.corporate.t-mobile.com/artifactory/eos-release-local</url>
        </repository>
        <snapshotRepository>
            <id>snapshots</id>
            <name>prd-art-jav-1a.corporate.t-mobile.com-snapshots</name>
            <url>https://artifactory.corporate.t-mobile.com/artifactory/eos-snapshot-local</url>
        </snapshotRepository>
    </distributionManagement>

</project>
